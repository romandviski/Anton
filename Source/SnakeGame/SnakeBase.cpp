// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "SmallWall.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.5f;
	LastMoveDirection = EMovementDirection::DOWN;
	CurrentMoveDirection = EMovementDirection::DOWN;
	Health = 100.f;
	Score = 0;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation;
		//Bug fix. We ignore snake creation and then add new elem to the tail-end location
		if (SnakeElements.Num() <= 4)
		{
			NewLocation = FVector(SnakeElements.Num() * ElementSize, 0, 80);
		}
		else
		{
			NewLocation = SnakeElements[SnakeElements.Num()-1]->GetActorLocation();
			AddHealth();
			AddScore();
		}
		FTransform NewTransform(NewLocation);

		auto NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
		
	}
	
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);



	switch (LastMoveDirection)
	{
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y += ElementSize;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y -= ElementSize;
			break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	LastMoveDirection = CurrentMoveDirection;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);

		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SetMovementSpeed(float NewSpeed)
{
	MovementSpeed = NewSpeed;
	SetActorTickInterval(MovementSpeed);
}


void ASnakeBase::AddHealth(float value)
{
	if (Health + value > 100)
	{
		Health = 100;
	}
	else
	{
		Health += value;
	}
}

void ASnakeBase::AddScore(float value)
{
	Score += value;
}

void ASnakeBase::ToggleWallCollision()
{
	TSubclassOf<ASmallWall> classToFind = ASmallWall::StaticClass();
	TArray<AActor*> foundWalls;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), classToFind, foundWalls);

		for (int i = 0; i < foundWalls.Num(); i++)
		{
			auto Wall = Cast<ASmallWall>(foundWalls[i]);

			if (Wall->MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
			{
				Wall->MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
				Wall->MaxToggleNumber += 13;
			}
			else
			{
				Wall->MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			}
		}
	
}

void ASnakeBase::SetWallCollisionTimer()
{
	GetWorld()->GetTimerManager().SetTimer(TimerHandler, this, &ASnakeBase::ToggleWallCollision, 5.f, false);
}

