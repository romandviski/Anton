// Fill out your copyright notice in the Description page of Project Settings.


#include "Arena.h"
#include "Food.h"
#include "FoodBonus.h"
#include "SpeedBonus.h"
#include "SlowDownBonus.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "SmallWall.h"
#include "Kismet/KismetSystemLibrary.h"
#include "WallBonus.h"

// Sets default values
AArena::AArena()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpawnCoorinate_X = 10;
	SpawnCoorinate_Y = 10;
	SpawnCoorinate_Z = 80;
	Padding			 = 200;
}

// Called when the game starts or when spawned
void AArena::BeginPlay()
{
	Super::BeginPlay();
	SpawnEatableActor();


}

// Called every frame
void AArena::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (!IsValid(SpawnedActorPtr))
	{
		SpawnEatableActor();
	}
	
	if (!(IsValid(SpawnedWallPtr)))
	{
		int rnd = FMath::RandRange(8, 16);
		for (int i = 0; i < rnd; i++)
		{
			SpawnSmallWall();
		}
	}
	
	
}


void AArena::SpawnEatableActor()
{
	FVector NewLocation = FVector(0);
	FVector SpawnLocation = this->GetActorLocation();
	
	int X = FMath::RandRange(0, SpawnCoorinate_X - 1);
	int Y = FMath::RandRange(0, SpawnCoorinate_Y - 1);
	
	
	NewLocation = FVector(SpawnLocation.X + X * Padding, SpawnLocation.Y + Y * Padding, SpawnCoorinate_Z);

	if (IsImpossibleToSpawn(NewLocation, 200, 1000))
	{
		return;
	}

	else
	{
		FTransform NewTransform(NewLocation);


		int rnd = FMath::RandRange(0, 15);
		switch (rnd)
		{
		case 0:
			SpawnedActorPtr = GetWorld()->SpawnActor<AFoodBonus>(FoodBonusClass, NewTransform);
			break;
		case 1:
			SpawnedActorPtr = GetWorld()->SpawnActor<ASpeedBonus>(SpeedBonusClass, NewTransform);
			break;
		case 2:
			SpawnedActorPtr = GetWorld()->SpawnActor<ASlowDownBonus>(SlowDownBonusClass, NewTransform);
			break;
		case 3:
			SpawnedActorPtr = GetWorld()->SpawnActor<AWallBonus>(WallBonusClass, NewTransform);
			break;
		default:
			SpawnedActorPtr = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
			break;
		}
	}
	
	
	
}


bool AArena::IsImpossibleToSpawn(FVector NewSpawnLocation, int MinPossibleDistance, int MaxPossibleDistance)
{
	bool Impossible = false;

	if (IsValid(Snake))
	{
		FHitResult HitRes;
		UKismetSystemLibrary::LineTraceSingle(GetWorld(), NewSpawnLocation,
			NewSpawnLocation + FVector(0, 0, 200), ETraceTypeQuery::TraceTypeQuery1,
			false, TArray<AActor*>{}, EDrawDebugTrace::None, HitRes, true);

		if (HitRes.bBlockingHit)
		{
			Impossible = true;
		}
		
		else if (FVector::Dist(Snake->GetActorLocation(), NewSpawnLocation) <= MinPossibleDistance ||
				 FVector::Dist(Snake->GetActorLocation(), NewSpawnLocation) >= MaxPossibleDistance)
		{
			Impossible = true;
		}
		else
		{
			for (int i = 0; i < Snake->SnakeElements.Num(); i++)
			{
				if (FVector::Dist(Snake->SnakeElements[i]->GetActorLocation(), NewSpawnLocation) <= MinPossibleDistance)
				{
					Impossible = true;
				}
			}
		}

	}
	
	return Impossible;
}


void AArena::SetSnakeActor(ASnakeBase* SnakeActor)
{
	Snake = SnakeActor;
}

void AArena::SpawnSmallWall()
{
	FVector NewLocation;
	FVector SpawnLocation = this->GetActorLocation();

	int X = FMath::RandRange(0, SpawnCoorinate_X - 1);
	int Y = FMath::RandRange(0, SpawnCoorinate_Y - 1);


	NewLocation = FVector(SpawnLocation.X + X * Padding, SpawnLocation.Y + Y * Padding, SpawnCoorinate_Z);

	
	if (IsImpossibleToSpawn(NewLocation, 400, 2000))
	{
		return;
	}

	else
	{

		FTransform NewTransform(NewLocation);

		SpawnedWallPtr = GetWorld()->SpawnActor<ASmallWall>(SmallWallClass, NewTransform);
	}
	
	
	

}



