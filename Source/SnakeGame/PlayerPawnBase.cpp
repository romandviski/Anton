// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
#include "Arena.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

	ArenaSpawnCoordinate_X = -700;
	ArenaSpawnCoordinate_Y = -900;
	ArenaSpawnCoordinate_Z =  110;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));

	CreateArena();
	CreateSnakeActor();
	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	
	DecreaseHealth();
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);

}

void APlayerPawnBase::CreateSnakeActor()
{
	FVector newVector = FVector(Arena->SpawnCoorinate_X / 2 * Arena->Padding, Arena->SpawnCoorinate_Y / 2 * Arena->Padding, Arena->SpawnCoorinate_Z);

	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform(newVector));
	Arena->SetSnakeActor(SnakeActor);
	
}

void APlayerPawnBase::CreateArena()
{
	FVector newVector = FVector(ArenaSpawnCoordinate_X, ArenaSpawnCoordinate_Y, ArenaSpawnCoordinate_Z);
	Arena = GetWorld()->SpawnActor<AArena>(ArenaClass, FTransform(newVector));
	
}


void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		{
			SnakeActor->CurrentMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->CurrentMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->CurrentMoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->CurrentMoveDirection = EMovementDirection::LEFT;
		}
	}
}

void APlayerPawnBase::DecreaseHealth(float value)
{
	if (IsValid(SnakeActor))
	{
		if (SnakeActor->Health - value < 0)
		{
			SnakeActor->Health = 0;
			SnakeActor->Destroy();
		}
		else
		{
			SnakeActor->Health -= value;
		}
	}
	
}






